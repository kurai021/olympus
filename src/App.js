import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faSquareTwitter, faDiscord } from '@fortawesome/free-brands-svg-icons'
import { faCompass } from '@fortawesome/free-solid-svg-icons'

import "./App.css";

import Home from './views/Home'
import Auctions from './views/Auctions'
import Raffles from './views/Raffles'
import Market from './views/Market'
import Merch from './views/Merch'
import Admin from './views/Admin'
import RegisterItem from './views/RegisterItem'

library.add(fas, faSquareTwitter, faDiscord, faCompass)

function App() {

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/auctions" element={<Auctions />} />
          <Route path="/raffles" element={<Raffles />} />
          <Route path="/market" element={<Market />} />
          <Route path="/merch" element={<Merch />} />
          <Route path="/admin" element={<Admin />}></Route>
          <Route path="/register-ITEM" element={<RegisterItem />}></Route>
        </Routes>
        
      </BrowserRouter>
    </>
  );
}

export default App;
