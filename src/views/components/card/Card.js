import React, { useState} from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Image from 'react-bootstrap/Image';
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Modal from 'react-bootstrap/Modal';
import InputGroup from "react-bootstrap/InputGroup";
import Form from "react-bootstrap/Form";

import { useLocation } from "react-router-dom";

import "./Card.css"

export default function Cards(props){
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const location = useLocation()

    return (
        <>
        <Col style={{ padding: "1.5rem" }}>
            <Card className="card">
                <Card.Img variant="top" src={props.block.image} />
                <Card.Body>
                    <Card.Title>{props.block.name}</Card.Title>
                    {(() => {
                        switch(location.pathname){
                            case "/raffles":
                                return (
                                    <>
                                    <Card.Text as={"div"}>
                                        <Row>
                                            <Col>
                                                <small>Entries</small><br/>
                                                {props.block.entries}/{props.block.totalEntries}
                                            </Col>
                                            <Col>
                                                <small>Raffle ends in</small><br/>
                                                {props.block.raffleEnds}
                                            </Col>
                                        </Row>
                                    </Card.Text>
                                    {(() => {
                                        if (props.block.raffleEnds === "Ended") {
                                            return (
                                                <Button variant="danger"  onClick={handleShow}>Result</Button>
                                            )
                                        }
                                        else {
                                            return (
                                                <Button variant="primary"  onClick={handleShow}>Join Raffle</Button>
                                            )
                                        }
                                    })()}
                                </>
                                )
                            case "/market":
                                return (
                                    <>
                                        <Card.Text as={"div"}>
                                            <Row>
                                                <Col>
                                                    <small>Price</small><br/>
                                                    {props.block.price} LCD
                                                </Col>
                                            </Row>
                                        </Card.Text>
                                        <Button variant="primary"  onClick={handleShow}>Buy</Button>
                                    </>
                                )
                            case "/merch":
                                return (
                                    <>
                                        <Card.Text as={"div"}>
                                            <Row>
                                                <Col>
                                                    <small>Price</small><br/>
                                                    {props.block.price} LCD
                                                </Col>
                                            </Row>
                                        </Card.Text>
                                        <Button variant="primary"  onClick={handleShow}>Buy</Button>
                                    </>
                                )
                            default:
                                return (
                                <>
                                    <Card.Text as={"div"}>
                                        <Row>
                                            <Col>
                                                <small>Latest Bid</small><br/>
                                                {props.block.latestBid}
                                            </Col>
                                            <Col>
                                                <small>Auction ends in</small><br/>
                                                {props.block.auctionEnds}
                                            </Col>
                                        </Row>
                                    </Card.Text>
                                    {(() => {
                                        if (props.block.auctionEnds === "Ended") {
                                            return (
                                                <Button variant="danger"  onClick={handleShow}>Ended</Button>
                                            )
                                        }
                                        else {
                                            return (
                                                <Button variant="primary"  onClick={handleShow}>Bid Up</Button>
                                            )
                                        }
                                    })()}
                                </>
                            )
                        }
                    })()}
                </Card.Body>
            </Card>
        </Col>

        <Modal
            show={show}
            onHide={handleClose}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title>{props.block.name}</Modal.Title>
            </Modal.Header>
            {(() => {
                switch(location.pathname){
                    case "/raffles":
                        return (
                            <>
                            <Modal.Body>
                                <Container>
                                    <Row>
                                        <Col sm={12} md={4} lg={4} xl={4} xxl={4}>
                                            <Image fluid src={props.block.image} />
                                        </Col>
                                        <Col sm={12} md={8} lg={8} xl={8} xxl={8}>
                                            <Row>
                                                <Col>{props.block.description}</Col>
                                            </Row>
                                            {(() => {
                                                if (props.block.raffleEnds !== "Ended") {
                                                    <Row>
                                                        <Col>
                                                            <span>
                                                                <strong>Winner: </strong>5GgmUAGhW8WeoyLr83RNxWnFus8ntSuccaD8yWRd35dE
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                }
                                            })()}
                                            <Row className="centeredDetails">
                                                <Col>
                                                    <small>Entries</small>
                                                    <br />
                                                    {props.block.entries}/{props.block.totalEntries}
                                                </Col>
                                                <Col>
                                                    <small>Price</small>
                                                    <br />
                                                    {props.block.priceTicket} per ticket
                                                </Col>
                                                <Col>
                                                    <small>Raffle ends in</small>
                                                    <br />
                                                    {props.block.raffleEnds}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                {(() => {
                                    if (props.block.raffleEnds !== "Ended") {
                                        return (
                                            <Row>
                                                <Col md={{ offset: 1 }}>
                                                    <InputGroup className="mb-3">
                                                        <Button variant="outline-secondary">-</Button>
                                                        <Form.Control
                                                        className="centered"
                                                        placeholder="1"
                                                        aria-label="Join"
                                                        aria-describedby="joinRaffle"
                                                        />
                                                        <Button variant="outline-secondary">+</Button>
                                                        <Button
                                                        variant="primary"
                                                        id="joinRaffle"
                                                        onClick={handleClose}
                                                        >
                                                            Join Raffle
                                                        </Button>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                        );
                                    }
                                })()}
                            </Modal.Footer>
                            </>
                        )
                    case "/market":
                        return (
                            <>
                            <Modal.Body>
                                <Container>
                                    <Row>
                                        <Col sm={12} md={4} lg={4} xl={4} xxl={4}>
                                            <Image fluid src={props.block.image} />
                                        </Col>
                                        <Col sm={12} md={8} lg={8} xl={8} xxl={8}>
                                            <Row>
                                                <Col>{props.block.description}</Col>
                                            </Row>
                                            <Row className="centeredDetails">
                                                <Col>
                                                    <small>Price</small>
                                                    <br />
                                                    {props.block.price}
                                                </Col>
                                                <Col>
                                                    <small>Existing Units</small>
                                                    <br />
                                                    {(() => {
                                                        if (props.block.units === 1){
                                                            return(<>{props.block.units} unit</>)
                                                        }
                                                        else {
                                                            return(<>{props.block.units} units</>)
                                                        }
                                                    })()}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Row>
                                    <Col md={{ offset: 1 }}>
                                        <InputGroup className="mb-3">
                                            <Button variant="outline-secondary">-</Button>
                                            <Form.Control
                                                className="centered"
                                                placeholder="1"
                                                aria-label="Buy"
                                                aria-describedby="Buy"
                                            />
                                            <Button variant="outline-secondary">+</Button>
                                            <Button
                                                variant="primary"
                                                id="joinRaffle"
                                                onClick={handleClose}
                                            >
                                                Buy
                                            </Button>
                                        </InputGroup>
                                    </Col>
                                </Row>
                            </Modal.Footer>
                            </>
                        )
                    case "/merch":
                        return (
                            <>
                            <Modal.Body>
                                <Container>
                                    <Row>
                                        <Col sm={12} md={4} lg={4} xl={4} xxl={4}>
                                            <Image fluid src={props.block.image} />
                                        </Col>
                                        <Col sm={12} md={8} lg={8} xl={8} xxl={8}>
                                            <Row>
                                                <Col>{props.block.description}</Col>
                                            </Row>
                                            <Row className="centeredDetails">
                                                <Col>
                                                    <small>Price</small>
                                                    <br />
                                                    {props.block.price}
                                                </Col>
                                                <Col>
                                                    <small>Existing Units</small>
                                                    <br />
                                                    {(() => {
                                                        if (props.block.units === 1){
                                                            return(<>{props.block.units} unit</>)
                                                        }
                                                        else {
                                                            return(<>{props.block.units} units</>)
                                                        }
                                                    })()}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Row>
                                    <Col md={{ offset: 1 }}>
                                        <InputGroup className="mb-3">
                                            <Button variant="outline-secondary">-</Button>
                                            <Form.Control
                                                className="centered"
                                                placeholder="1"
                                                aria-label="Buy"
                                                aria-describedby="Buy"
                                            />
                                            <Button variant="outline-secondary">+</Button>
                                            <Button
                                                variant="primary"
                                                id="joinRaffle"
                                                onClick={handleClose}
                                            >
                                                Buy
                                            </Button>
                                        </InputGroup>
                                    </Col>
                                </Row>
                            </Modal.Footer>
                            </>
                        )
                    default:
                        return (
                        <>
                            <Modal.Body>
                                <Container>
                                    <Row>
                                        <Col sm={12} md={4} lg={4} xl={4} xxl={4}>
                                            <Image fluid src={props.block.image} />
                                        </Col>
                                        <Col sm={12} md={8} lg={8} xl={8} xxl={8}>
                                            <Row>
                                                <Col>{props.block.description}</Col>
                                            </Row>
                                            {(() => {
                                                if (props.block.auctionEnds !== "Ended") {
                                                    <Row>
                                                        <Col>
                                                            <span>
                                                                <strong>Winner: </strong>5GgmUAGhW8WeoyLr83RNxWnFus8ntSuccaD8yWRd35dE
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                }
                                            })()}
                                            <Row className="centeredDetails">
                                                <Col>
                                                    <small>Latest Bid</small>
                                                    <br />
                                                    {props.block.latestBid}
                                                </Col>
                                                <Col>
                                                    <small>Auction ends in</small>
                                                    <br />
                                                    {props.block.auctionEnds}
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                {(() => {
                                    if (props.block.auctionEnds !== "Ended") {
                                        return (
                                            <Row>
                                                <Col md={{ offset: 1 }}>
                                                    <InputGroup className="mb-3">
                                                        <Form.Control
                                                        placeholder="Bid"
                                                        aria-label="Bid"
                                                        aria-describedby="bidUp"
                                                    />
                                                        <Button
                                                        variant="primary"
                                                        id="bidUp"
                                                        onClick={handleClose}
                                                        >
                                                            Bid Up
                                                        </Button>
                                                    </InputGroup>
                                                </Col>
                                            </Row>
                                        );
                                    }
                                })()}
                            </Modal.Footer>
                        </>
                    );
                }
            })()}
        </Modal>
        </>
    )
}
