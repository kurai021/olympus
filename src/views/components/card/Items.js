import React from "react";
import Card from './Card'

const Components = {
    card: Card
};

// eslint-disable-next-line import/no-anonymous-default-export
export default block => {
    // component does exist
    if (typeof Components[block.type] !== "undefined") {
        return React.createElement(Components[block.type], {
            key: block._uid,
            block: block
        });
    }

    // component doesn't exist yet
    return React.createElement(
        () => <div>The component {block.name} has not been created yet.</div>,
        { key: block._uid }
    );
}
