import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import InputGroup from "react-bootstrap/InputGroup";
import Form from "react-bootstrap/Form";

import Image from 'react-bootstrap/Image';
import HorizontalLogo from "../../assets/hor-logo.png"
import Button from "react-bootstrap/Button";

import './OlympusNavbar.css'

function OlympusNavbar(){

    const location = useLocation()

    //change nav color when scrolling
    const [color, setColor] = useState(false)
    const [flexColumn, setFlexColumn] = useState(false)
    const changeColor = () => {
        if (window.scrollY >= 90){
            setColor(true)
            setFlexColumn(true)
        }
        else {
            setColor(false)
            setFlexColumn(false)
        }
    }

    
    //const bg = useTransform(scrollYProgress, "dark", "light");

    window.addEventListener('scroll', changeColor)

    return(
        <Navbar bg={color ? 'dark': 'light'} variant={color ? 'dark': ''} expand="lg" fixed="top" style={{transition: "all 0.5s"}}>
                <Container fluid>
                    <Navbar.Brand as={Link} to="/">
                        <Image src={HorizontalLogo} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto"
                            defaultActiveKey={location.pathname}
                            activeKey={location.pathname}
                        >
                            <Nav.Item>
                                <Nav.Link as={Link} to="/" eventKey={"/"}>Home</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/auctions" eventKey={"/auctions"}>Auctions</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/raffles" eventKey={"/raffles"}>Raffles</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/market" eventKey={"/market"}>Market</Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link as={Link} to="/merch" eventKey={"/merch"}>Merch</Nav.Link>
                            </Nav.Item>
                        </Nav>
                        <Nav className={["ms-auto", flexColumn ? '': 'flex-column']} style={{transition: "all 0.5s"}}>
                            <Nav.Item>
                                <div className="d-grid gap-2">
                                    <Button variant="primary" size="lg" className="connect-wallet">Connect Wallet</Button>
                                </div>
                            </Nav.Item>
                            <Nav.Item>
                                <InputGroup size="lg">
                                    <InputGroup.Text id="lcd-balance">Balance</InputGroup.Text>
                                        <Form.Control
                                            placeholder="0.000 LCD"
                                            aria-label="Balance"
                                            aria-describedby="lcd-balance"
                                    />
                                </InputGroup>
                            </Nav.Item>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
    )
}

export default OlympusNavbar;