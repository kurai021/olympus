import React, { useEffect, useState } from "react";
import "./Home.css"
import { Link } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from 'react-bootstrap/Image';
import Offcanvas from "react-bootstrap/Offcanvas";
import Nav from "react-bootstrap/Nav";

import { motion } from "framer-motion"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Hipster from "./assets/hipster-in-stone.png"
import VerticalLogo from "./assets/vert-logo.png"

import MeIcon from "./assets/meIcon"

function Home() {
    const [show, setShow] = useState(false);
    useEffect(() => {
        const timer = setTimeout(() => {
            setShow(true)
        }, 5000);
        return () => clearTimeout(timer)
    }, [])

    return (
        <Container fluid className="containerHome backgroundMove">
            <Row>
                <Col>
                <motion.div 
                    initial={{ x: "-100%" }}
                    animate={{ x: "0%" }}
                    transition={{
                        duration: 2,
                        delay: 3,
                        bounce: 1
                    }}
                    style={{
                        position: "fixed",
                        bottom: "0px"
                    }}
                >
                    <Col md={12} lg={8}>
                        <Image src={Hipster} fluid={true} />
                    </Col>
                </motion.div>
                    <Offcanvas
                        show={show}
                        placement={"end"}
                        backdrop={false}
                    >
                        <Offcanvas.Header closeButton={false}>
                            <Offcanvas.Title>
                                <Image src={VerticalLogo} width={"100px"} />
                                <h4>Menu</h4>
                            </Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                            <Nav defaultActiveKey="/" className="flex-column">
                                <Nav.Link as={Link} to="/auctions">
                                    <h1>Auctions</h1>
                                </Nav.Link>
                                <Nav.Link as={Link} to="/raffles">
                                    <h1>Raffles</h1>
                                </Nav.Link>
                                <Nav.Link as={Link} to="/market">
                                    <h1>Marketplace</h1>
                                </Nav.Link>
                                <Nav.Link as={Link} to="/merch">
                                    <h1>Merchandise</h1>
                                </Nav.Link>
                            </Nav>
                            <hr className="separator" />
                            <Nav className="justify-content-center">
                                <a className="nav-link" href="https://twitter.com/LCDLabNFT" target={"_blank"} rel="noreferrer">
                                    <FontAwesomeIcon icon={['fab','square-twitter']} size="3x" />
                                </a>
                                <a className="nav-link" href="https://discord.com/invite/VvErr8MnZW" target={"_blank"} rel="noreferrer">
                                    <FontAwesomeIcon icon={['fab','discord']} size="3x" />
                                </a>
                                <a className="nav-link" href="https://magiceden.io/marketplace/the_stone_heads" target={"_blank"} rel="noreferrer">
                                    <MeIcon />
                                </a>
                                <a className="nav-link" href="https://thestoneheads.com/" target={"_blank"} rel="noreferrer">
                                    <FontAwesomeIcon icon={['fas','compass']} size="3x" />
                                </a>
                            </Nav>
                        </Offcanvas.Body>
                    </Offcanvas>
                </Col>
            </Row>
        </Container>
    );
}

export default Home;
