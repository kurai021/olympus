import React from "react";
import "./Raffles.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";

import OlympusNavbar from './components/navbar/OlympusNavbar'
import Items from './components/card/Items'

const data = {
    content: {
        body: [
            {
                _uid: "01",
                name:"The Stone Heads #1180",
                type: "card",
                image: "https://api.phantom.app/image-proxy/?image=https%3A%2F%2Fbafybeiesimyqlatgdqk6zstbwb5t5sulic4qtcwvb5o3vwitlpope76crm.ipfs.nftstorage.link%2F1180.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "3 days"
            },
            {
                _uid: "02",
                name:"The Stone Heads #1270",
                type: "card",
                image: "https://api.phantom.app/image-proxy/?image=https%3A%2F%2Fbafybeihky45lonluroodhuzgmlqu55rvj62svnktrm4gylxlqsngagl5kq.ipfs.nftstorage.link%2F1270.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "03",
                name:"The Stone Heads #1404",
                type: "card",
                image: "https://api.phantom.app/image-proxy/?image=https%3A%2F%2Fbafybeieijjy7sbebmvidmiwxwbtffxkvl6igr46dkbj2s5xbla7fis6uxm.ipfs.nftstorage.link%2F1404.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "04",
                name:"The Stone Heads #3750",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeihnel5rmq5gc6mr72e5rklgke33rhi2u7yconxgbjpbox5ygz7h6i.ipfs.nftstorage.link%2F3750.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "05",
                name:"The Stone Heads #3477",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeie6vqf7lmmnztyzjzahe6rvstsqoyc27emubnss2nbcqzeawh2f5i.ipfs.nftstorage.link%2F3477.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "06",
                name:"The Stone Heads #733",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeidl54izwyef7vjhgcgswva752ulxmk3garonml3l3im44vqoy6pla.ipfs.nftstorage.link%2F733.png%3Fext%3Dpng",
                description:"Stone Heads, the collection of 5555 unique NFTs by Leo Caillard, launched on the Solana blockchain, represents a connection between traditional and contemporary art, bringing iconic characters from the mythology to the Metaverse.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "07",
                name:"Iconic Davids #80",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https://nftstorage.link/ipfs/bafybeiaxdho3cu7c7h3j6sg4y7egd7qf3jf6usugykwfppac6nbeu3hfoq/79.png",
                description:"Iconic Davids, the collection of 333 unique NFTs by LCD Lab and Leo Caillard, is the iconic mythological face of the Stone Heads collection as well as the revolutionary cultural symbol of the modern Digital Renaissance powered by Web3. Iconic Davids serve as your golden digital pass to David DAO revenue sharing and all things LCD Lab.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "08",
                name:"Iconic Davids #123",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https://nftstorage.link/ipfs/bafybeie7j5imbsr3zvttz25sr7erxd7g4exmzvicx3stqg5gsjvg5w3e4i/122.png",
                description:"Iconic Davids, the collection of 333 unique NFTs by LCD Lab and Leo Caillard, is the iconic mythological face of the Stone Heads collection as well as the revolutionary cultural symbol of the modern Digital Renaissance powered by Web3. Iconic Davids serve as your golden digital pass to David DAO revenue sharing and all things LCD Lab.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "09",
                name:"Iconic Davids #18",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https://nftstorage.link/ipfs/bafybeiajuek3rro6g3kwiyrchoxrdr3uz3agttmq6zkto33gvciolhhsa4/17.png",
                description:"Iconic Davids, the collection of 333 unique NFTs by LCD Lab and Leo Caillard, is the iconic mythological face of the Stone Heads collection as well as the revolutionary cultural symbol of the modern Digital Renaissance powered by Web3. Iconic Davids serve as your golden digital pass to David DAO revenue sharing and all things LCD Lab.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "10",
                name:"Banksy Radar Rats #756",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeiheqmsqw6mbiqu5xpyapzrdst2ek5wz47lnq7qs5tw7zbqiqmzwxe.ipfs.nftstorage.link%2F756.png%3Fext%3Dpng",
                description:"Enter the most exclusive club for blue-chip art on Solana or burn your NFT to get an authenticated Banksy. This NFT collection has been designed by LCD Lab creative team. Banksy is not involved in these creations. The collateral for each NFT is an authenticated Banksy art piece from the Banksy Walled-Off Hotel in Bethlehem, Palestine. All pieces come with a certificate from the hotel and have been certified by Jean-Marc Scialom contemporary art expert in Drouot Auction House, Paris, France.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "11",
                name:"Banksy Radar Rats #303",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeiftacnc7r6cv4vzswvmiz5cecjbgydoldvaqn45sccj6tobgrq3sy.ipfs.nftstorage.link%2F303.png%3Fext%3Dpng",
                description:"Enter the most exclusive club for blue-chip art on Solana or burn your NFT to get an authenticated Banksy. This NFT collection has been designed by LCD Lab creative team. Banksy is not involved in these creations. The collateral for each NFT is an authenticated Banksy art piece from the Banksy Walled-Off Hotel in Bethlehem, Palestine. All pieces come with a certificate from the hotel and have been certified by Jean-Marc Scialom contemporary art expert in Drouot Auction House, Paris, France.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
            {
                _uid: "12",
                name:"Banksy Radar Rats #470",
                type: "card",
                image: "https://img-cdn.magiceden.dev/rs:fill:640:640:0:0/plain/https%3A%2F%2Fbafybeig56crqwfx5ppanfbfy65ziwhbewywgaf6zijwytg52sspvspidda.ipfs.nftstorage.link%2F470.png%3Fext%3Dpng",
                description:"Enter the most exclusive club for blue-chip art on Solana or burn your NFT to get an authenticated Banksy. This NFT collection has been designed by LCD Lab creative team. Banksy is not involved in these creations. The collateral for each NFT is an authenticated Banksy art piece from the Banksy Walled-Off Hotel in Bethlehem, Palestine. All pieces come with a certificate from the hotel and have been certified by Jean-Marc Scialom contemporary art expert in Drouot Auction House, Paris, France.",
                entries: 1,
                totalEntries:1000,
                raffleEnds: "Ended"
            },
        ]
    }
}

function Raffles(){
    return (
        <>
            <OlympusNavbar />
            <Container fluid className="containerRaffles">
                <Row xs={1} sm={2} md={3} lg={4} xl={6} xxl={8} className="cardContainer">
                    {data.content.body.map(block => Items(block))}
                </Row>
            </Container>
        </>
    );
}

export default Raffles;