import React from 'react';
import './Admin.css'

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Stack from 'react-bootstrap/Stack';

function Admin(){
    return (
        <Container className="containerAdmin">
            <Row className="justify-content-md-center align-items-center">
                <Col md={6}>
                    <Tabs
                        defaultActiveKey="register"
                        id="uncontrolled-tab-example"
                        className="mb-3"
                        fill
                    >
                        <Tab eventKey="register" title="Register">
                            <Form>
                                <Stack>
                                    <Form.Group className="mb-3" controlId="registerEmail">
                                        <Form.Label>Email address</Form.Label>
                                        <Form.Control type="email" placeholder="Enter email" />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="registerUser">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control type="text" placeholder="Enter username" />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="registerPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>

                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Stack>
                            </Form>
                        </Tab>
                        
                        <Tab eventKey="login" title="Login">
                            <Form>
                                <Stack>
                                    <Form.Group className="mb-3" controlId="loginUser">
                                        <Form.Label>Username</Form.Label>
                                        <Form.Control type="text" placeholder="Enter username" />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="loginPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>

                                    <Button variant="primary" type="submit">
                                        Submit
                                    </Button>
                                </Stack>
                            </Form>
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
        </Container>
    );
}

export default Admin