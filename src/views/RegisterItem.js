import React, { useState} from 'react'

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from "react-bootstrap/Form";
import Stack from 'react-bootstrap/Stack';

import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import InputGroup from 'react-bootstrap/InputGroup';

function RegisterItem(){
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [showDateEnd, setDateEnd] = useState(null)
    
    const [selectToken, setSelectToken] = useState('Select Token')
    const handleSelectToken=(e)=>{
        setSelectToken(e)
    }

    function dateEnd(){
        if(showDateEnd === "raffle" || showDateEnd === "auction"){
            return (
                <Form.Group className="mb-3" controlId="endDate">
                    <Form.Label>End Date</Form.Label>
                    <Form.Control type="datetime-local" />
                </Form.Group>
            )
        }
        else {
            return(
                <></>
            )
        }
    }
    
    return(
        <Container>
            <Row>
                <Col>
                    <Button variant="primary" onClick={handleShow}>Register Item</Button>

                    <Modal
                        show={show}
                        onHide={handleClose}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title>Register Item</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Container>
                                <Row>
                                    <Col>
                                        <Form>
                                            <Stack>
                                                <Form.Group className="mb-3" controlId="mintID">
                                                    <Form.Label>Mint ID</Form.Label>
                                                    <Form.Control type="text" placeholder="8AiA5VMzV2fBVu7grYffuFWSHEvDX7uduhwLrGgEgLSV" />
                                                </Form.Group>

                                                <Form.Group className="mb-3" controlId="typeItem">
                                                    <Form.Label>Type</Form.Label>
                                                    <Form.Select
                                                        defaultValue="market"
                                                        onClick={(event) => {
                                                            // here set target value to state which is 0, 1, 2, 3
                                                            setDateEnd(event.target.value);
                                                        }}
                                                    >
                                                        <option value="raffle">Raffle</option>
                                                        <option value="auction">Auction</option>
                                                        <option value="merch">Merch</option>
                                                        <option value="market">Market</option>
                                                    </Form.Select>
                                                </Form.Group>

                                                {dateEnd()}

                                                <Form.Label>Price</Form.Label>
                                                <InputGroup className="mb-3">
                                                    <Form.Control aria-label="Price" />
                                                    <DropdownButton
                                                        variant="secondary"
                                                        title={selectToken}
                                                        id="input-group-dropdown-2"
                                                        align="end"
                                                        onSelect={handleSelectToken}
                                                    >
                                                        <Dropdown.Item eventKey="LCD">LCD</Dropdown.Item>
                                                        <Dropdown.Item eventKey="SOL">SOL</Dropdown.Item>
                                                    </DropdownButton>
                                                </InputGroup>

                                                <Form.Group className="mb-3" controlId="tokensAvailable">
                                                    <Form.Label>Tokens Available</Form.Label>
                                                    <Form.Control type="text" placeholder="10" />
                                                </Form.Group>

                                                <Button variant="primary" type="submit">
                                                    Submit
                                                </Button>
                                            </Stack>
                                        </Form>
                                    </Col>
                                </Row>
                            </Container>
                        </Modal.Body>
                    </Modal>
                </Col>
            </Row>
        </Container>
    )
}

export default RegisterItem